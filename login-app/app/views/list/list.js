var dialogModule = require("tns-core-modules/ui/dialogs");
var ObservableModule = require("tns-core-modules/data/observable");
var NameListViewModel = require("./name-list-view-model");

var page;

var nameList = new NameListViewModel([]);
var pageData = ObservableModule.fromObject({
  nameList: nameList,
  name: ""
});

exports.loaded = function(args) {
  page = args.object;
  page.bindingContext = pageData;
  nameList.empty();

  //GET/SELECT
  nameList
    .listAllName()
    .catch(function(error) {
      console.log(error);
      return Promise.reject();
    })
    .then(function(response) {
      if (response != "") {
        console.log("List All Users");
        // console.log(response);
      } else {
        console.log("Cannot List");
        //   dialogModule.alert({
        //     message: "Login fail, Please try again",
        //     okButtonText: "OK"
        //   });
      }
    });
};

//POST/INSERT
exports.add = function(args) {
  name = page.getViewById("txbName");
  // console.log(name.text);

  if (name.text.trim() === "") {
      dialogModule.alert({
          message: "Please enter name!",
          okButtonText: "OK"
      })
  } else{
    name.dismissSoftInput();
      nameList.add(name.text);
  }
  pageData.set("name", "");

  page.getViewById("txbName").text="";
};
