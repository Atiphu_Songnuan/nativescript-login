var config = require("../../shared/config");
var ObservableArray = require("data/observable-array").ObservableArray;
var http = require("tns-core-modules/http");

function NameListViewModel(items) {
  var viewModel = new ObservableArray(items);

  viewModel.listAllName = function() {
    return http
      .request({
        url: config.apiUrl + "listall",
        method: "GET",
        headers: { "Content-Type": "application/json" }
      })
      .then(
        response => {
          var res = response.content.toJSON();
          if (res.length == 0) {
            res = [""];
          }

          for (var i = 0; i < res.length; i++){
            // look for the entry with a matching `code` value
            // console.log(res[i]["ID"]);
            viewModel.push({
                id: res[i]["ID"],
                name: res[i]["name"]
            })
          }
          return res;
          // res = result;
        },
        e => {
          console.log(e);
        }
      );
  };

  viewModel.empty = function() {
    while (viewModel.length) {
      viewModel.pop();
    }
  };

  viewModel.add = function(name) {
    console.log(name);
      viewModel.push({
          id:"",
          name: name,
          language: "Thai"
      });
  };

  return viewModel;
}

module.exports = NameListViewModel;
