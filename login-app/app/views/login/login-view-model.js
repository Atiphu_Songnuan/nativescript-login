var config = require("../../shared/config");
var observableModule = require("tns-core-modules/data/observable");
var http = require("tns-core-modules/http");

function User(info) {
  // console.log(JSON.stringify(info));
  info = info || {};

  // You can add properties to observables on creation
  var viewModel = new observableModule.fromObject({
    perid: info.perid || "",
    pin: info.pin || ""
  });

  var result = new Array();
  viewModel.login = function() {
    // console.log(viewModel);
    // console.log(config.apiUrl + "login");
    // console.log("email:" + viewModel.get("email") + ", password:" + viewModel.get("password"));
    return http
      .request({
        url: config.apiUrl + "login",
        method: "POST",
        headers: { "Content-Type": "application/json" },
        content: JSON.stringify({
          perid: viewModel.get("perid"),
          pin: viewModel.get("pin")
        })
      })
      .then(
        response => {
          var res = response.content.toJSON();
          if (res.length == 0) {
            result = {
              statusCode: 400,
              data: res[0]
            };
          } else {
            result = {
              statusCode: 200,
              data: res[0]
            };
          }
          return result;
        },
        e => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e)
          };
          return result;
        }
      );

    // http
    //   .request({
    //     url: config.apiUrl + "login",
    //     method: "POST",
    //     headers: { "Content-Type": "application/json" },
    //     content: JSON.stringify({ email: viewModel.get("email"), password: viewModel.get("password") })
    //   })
    //   .then(
    //     function(result) {
    //       console.log(result);
    //     },
    //     function(error) {
    //       console.error(JSON.stringify(error));
    //     }
    //   );

    // fetchModule
    //   .fetch(config.apiUrl + "login", {
    //     method: "POST",
    //     body: JSON.stringify({
    //       username: viewModel.get("email"),
    //       password: viewModel.get("password")
    //     }),
    //     headers: getCommonHeaders()
    //   })
    //   .then(handleErrors)
    //   .then(function(response) {
    //     console.log(response.json);
    //     // return response.json();
    //   });
    //   // .then(function(data) {
    //   //   console.log("tokenL " + data._kmd.authtoken);
    //   //   config.token = data._kmd.authtoken;
    //   // });
  };

  //   viewModel.register = function() {
  //     return fetchModule
  //       .fetch(config.apiUrl + "user/" + config.appKey, {
  //         method: "POST",
  //         body: JSON.stringify({
  //           username: viewModel.get("email"),
  //           email: viewModel.get("email"),
  //           password: viewModel.get("password")
  //         }),
  //         headers: getCommonHeaders()
  //       })
  //       .then(handleErrors);
  //   };

  return viewModel;
}

// function getCommonHeaders() {
//   return {
//     "Content-Type": "application/json"
//     // Authorization: config.appUserHeader
//   };
// }

// function handleErrors(response) {
//   if (!response.ok) {
//     // console.log(JSON.stringify(response));
//     throw Error("EEEE: " + response.statusText);
//   }
//   return response;
// }

module.exports = User;
