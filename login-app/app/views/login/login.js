// var CryptoJS = require("crypto-js");
// const base64url = require("base64url");

var frameModule = require("tns-core-modules/ui/frame");
// var observableModule = require("tns-core-modules/data/observable");
var LoginViewModel = require("./login-view-model");
var dialogModule = require("tns-core-modules/ui/dialogs");
var nstoasts = require("nativescript-toasts");

var user = new LoginViewModel({
    perid: "46773",
    pin: "2071"
});

var page;

exports.loaded = function(args) {
    page = args.object;
    page.bindingContext = user;
    // args.cancel = true; // Back button clicked to close app
};

exports.signIn = function() {
    // email = page.getViewById("email");
    // console.log(email.text);
    user.login()
        .catch(function(error) {
            console.log(error);
            return Promise.reject();
        })
        .then(function(response) {
            // console.log(response.statusCode);
            if (response.statusCode == "200") {
                console.log("Login success, Welcome");

                // dialogModule.alert({
                //   message: "Login Success",
                //   okButtonText: "OK",
                //   cancelable: false
                // });

                var options = {
                    text: "Login success",
                    duration: nstoasts.DURATION.SHORT,
                    position: nstoasts.POSITION.BOTTOM //optional
                };
                nstoasts.show(options);

                // console.log(response.data);
                global.UserData = response.data;
                // global.UserData.position = response.data.position;
                // global.UserData.email = response.data.email;

                const navigationEntry = {
                    moduleName: "views/bottomnavigation/bottomnavigation",
                    // moduleName: "views/bottomnavigation",
                    context: { info: "Login Page" },
                    animated: true,
                    transition: {
                        name: "slideLeft",
                        duration: 380,
                        curve: "easeIn"
                    }
                };
                frameModule.topmost().navigate(navigationEntry);
            } else {
                console.log("Login failed, Please try again!");
                dialogModule.alert({
                    message: "Login fail, Please try again",
                    okButtonText: "OK"
                });
            }
        });
};

exports.register = function() {
    const navigationEntry = {
        moduleName: "../../views/register/register",
        context: { info: "something you want to pass to your page" },
        animated: true,
        transition: {
            name: "slideLeft",
            duration: 380,
            curve: "easeIn"
        }
    };
    frameModule.topmost().navigate(navigationEntry);
};

// function JWT(userdata) {

//     const payload = {
//         name: userdata.name,
//         position: userdata.position,
//         email: userdata.email
//     };

//     // const secret = process.SECRET;

//     // const secret = "NativeScript";
//     // const signedToken = sign(payload, secret, {
//     //     algorithm: "HS256",
//     //     expiresIn:"1h"
//     // });

//     // console.log(signedToken);

//     // var header = {
//     //     alg: "HS256",
//     //     typ: "JWT"
//     // };
//     // var encodedHeader = base64url(JSON.stringify(header));

//     // var data = {
//     //     name: userdata.name,
//     //     position: userdata.position,
//     //     email: userdata.email
//     // };
//     // var encodedData = base64url(JSON.stringify(data));

//     // var token = encodedHeader + "." + encodedData;

//     // var secret = "NativeScript !!!";
//     // var signature  = CryptoJS.HmacSHA256(token, secret);
//     // signature = base64url(signature);
//     // var signedToken  = token + "." + signature;
//     // console.log(signedToken);

// }
