var ObservableArray = require("data/observable-array").ObservableArray;
var config = require("../../shared/config");
var http = require("tns-core-modules/http");

function ProfileViewModel(items) {
    var viewModel = new ObservableArray(items);

    // viewModel.profileData = function() {
        
    // };
    return viewModel;
}
module.exports = ProfileViewModel;
