// var ObservableModule = require("tns-core-modules/data/observable");
// var ProfileViewModel = require("views/profile/profile-view-model");
var dialogModule = require("tns-core-modules/ui/dialogs");
var nstoasts = require("nativescript-toasts");
var page;

// var profile = new ProfileViewModel([]);
// var pageData = ObservableModule.fromObject({
//     profile: profile
// });

exports.loaded = function(args) {
    page = args.object;
    page.getViewById("name").text = global.UserData.NAME + " " +global.UserData.SURNAME;
    // page.getViewById("position").text = global.UserData.position;
    // page.getViewById("email").text = global.UserData.email;
    // console.log(global.UserData);
};

exports.checkin = function() {
    // dialogModule.alert({
    //     message: "Check in clicked!",
    //     okButtonText: "OK",
    //     cancelable: false
    // });

    var options = {
        text: "Check in clicked!",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM //optional
    };
    nstoasts.show(options);
};

exports.money = function() {
    var options = {
        text: "money clicked!",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM //optional
    };
    nstoasts.show(options);
};

exports.calendar = function(){
    var options = {
        text: "calendar clicked!",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM //optional
    };
    nstoasts.show(options);
}